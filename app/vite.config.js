import { defineConfig, loadEnv } from "vite";
import react from "@vitejs/plugin-react";



// https://vitejs.dev/config/
export default defineConfig(((command, mode) => {
    const env = loadEnv(mode, process.cwd(), '');
    const ASSET_URL = process.env.ASSET_URL || '';
    return {
        base: `/emil/`,
        plugins: [react()],
    }
}))
