import React from 'react';
import useResizeObserver from '@react-hook/resize-observer'

const Branding = ({children, short}) => {
    const [size, setSize] = React.useState(null);

    React.useLayoutEffect(() => setSize(
        document.querySelector('body').getBoundingClientRect()
    ), []);

    useResizeObserver(
        document.querySelector('body'), 
        entry => setSize(entry.contentRect)
    );

    return (
        <span className="text-4xl text-[#ffffff44] my-2.5 italic grow">
            {size?.width < 1080 ? short : children}
        </span>
    );    
};

export default Branding;