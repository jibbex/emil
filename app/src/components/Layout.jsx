import React from 'react';
import Header from './Header';

const Layout = ({children}) => (    
    <div className="min-h-screen flex flex-col justify-center">
        <Header />
       <div className="grow flex flex-col">{children}</div>        
    </div>
);

export default Layout;