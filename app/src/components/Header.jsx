import React from 'react';
import Navigation from './Navigation';
import Branding from './Branding';
import Wrapper from './Wrapper';


const Header = () => (        
    <header>
        <Wrapper margin="1em" className="flex">
            <Branding short="E.M.I.L.">
                Extended Mathematical Improving and Learning
            </Branding> 
            <Navigation />
        </Wrapper>
    </header>
);

export default Header;