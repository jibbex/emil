import React from 'react';
import { Link } from "react-router-dom";

const Navigation = () => {
    const isActive = path => window?.location.pathname === path;
    const buttonStyle = {backdropFilter: 'blur(8px)'};

    return (
        <>
            <Link to="/emil">
                <button syle={buttonStyle} className={`${isActive(`/emil/`) || isActive(`/emil/play`) ? 'bg-rose-800/25 transition-all' : 'bg-transparent'} text-4xl transition-all hover:bg-[#00000022] group inline-flex items-center rounded-2xl px-3 py-2 mx-1 font-medium text-white hover:text-opacity-100`}>
                    <img width={32} height="auto" src={`/emil/calculator.svg`} title="calculator.svg"/>
                </button>
            </Link>
            <Link to="/emil/settings">
                <button syle={buttonStyle} className={`${isActive(`/emil/settings`) ? 'bg-rose-800/25 transition-all' : 'bg-transparent'} text-4xl transition-all hover:bg-[#00000022] group inline-flex items-center rounded-2xl px-3 py-2 mx-1 font-medium text-white hover:text-opacity-100`}>
                    <img width={32} height="auto" src={`/emil/settings.svg`} title="settings.svg"/>
                </button>
            </Link>
        </>
    );
};

export default Navigation;