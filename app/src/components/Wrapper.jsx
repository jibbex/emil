import React from 'react';

const Wrapper = ({style, maxWidth, margin, children}) => (
    <div className="flex" style={{maxWidth, margin: margin ?? '0 auto', ...style}}>
        {children}
    </div>
);

export default Wrapper;