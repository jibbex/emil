import React from 'react';

const Button = React.forwardRef((props, ref) => {
    const {onClick, value, className} = props;
    const classes = 'transition-all hover:shadow-md hover:bg-[#00000022] rounded-3xl px-3 py-2 bg-[#ffffff10] shadow-xl text-4xl sm:text-4xl md:text-6xl text-[#333333AA] text-center backdrop-blur-md';
    const handleClick = () => onClick(value);

    return (
        <button ref={ref} onClick={handleClick} className={`${classes} ${className}`}>
            {value}
        </button>
    )
});

export default Button;