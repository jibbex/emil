import Home from './Home';
import Settings from './Settings';
import Play from './Play';

export { Home, Settings, Play };