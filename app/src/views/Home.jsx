import React from 'react';
import Layout from './../components/Layout';

const Home = ({onStart}) => (
    <Layout>
        <div className='flex justify-center items-center' style={{height: 'calc(100vh - 160px'}}>
            <button onClick={() => onStart()} className="transition-all text-4xl hover:text-[#ffffffba] hover:no-underline px-4 py-2 bg-gradient-to-r shadow-xl hover:bg-red-600 bg-red-500 from-transparent to-red-600 hover:shadow-md text-white font-medium rounded-2xl">
                Starten
            </button>
        </div>
    </Layout>
);

export default Home;
