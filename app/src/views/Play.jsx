import React, {useRef} from 'react';
import { Transition } from '@headlessui/react'
import { useTimeoutFn } from 'react-use'
import Layout from './../components/Layout';
import Button from './../components/Button';

const Play = ({tasks, setResult}) => {
    const [value, setValue] = React.useState(0);
    const [taskIndex, setTaskIndex] = React.useState(0);
    const [isShowing, setIsShowing] = React.useState(false)
    const [, , resetIsShowing] = useTimeoutFn(() => setIsShowing(true), 100)
    const classes = 'rounded-3xl px-3 py-2 bg-[#ffffff10] shadow-xl m-0 text-4xl sm:text-4xl md:text-6xl text-[#333333AA] text-center backdrop-blur-md';
    const isDigit = num => (num => num >= 0 && num <= 9)(parseInt(num));
    const refs = new Map([
        ['0', useRef()],
        ['1', useRef()],
        ['2', useRef()],
        ['3', useRef()],
        ['4', useRef()],
        ['5', useRef()],
        ['6', useRef()],
        ['7', useRef()],
        ['8', useRef()],
        ['9', useRef()],
        ['Backspace', useRef()],
        ['Enter', useRef()],
        ['+/-', useRef()],
    ]);
    
    const changeValue = React.useCallback(val => {
        setValue(value !== '0' && value !== 0 ? value + val : val);
    }, [value]);
    
    const removeDigit = React.useCallback(() => {
        setValue(v => {
            setValue(
                v.length > 1
                ? v.substring(0, v.length - 1)
                : 0
            );        
        })
    });  

    const handlePlusMinus = React.useCallback(() => setValue(value / -1));

    const handleSetResult = React.useCallback(() => {
        setResult(taskIndex, value);

        if (taskIndex < tasks.length) {
            setTaskIndex(taskIndex + 1);
            setValue(0);
        }
    });

    const handleKeyDown = ({key}) => {      
        console.log(key)      
        if (key === 'Backspace') {
            removeDigit();
        } else if (key === 'Enter') {
            handleSetResult();
        } else if (Number.isInteger(parseInt(key))) {
            changeValue(key);
        } else if (key === '+' || key === '-') {
            key = '+/-';
            handlePlusMinus();
        }

        if (key === '+/-' || key === 'Backspace' || key === 'Enter' || isDigit(key)) {
            refs.get(key).current.classList.add('bg-[#00000022]', 'hover:shadow-md')
        }
    };

    const handleKeyUp = ({key}) => {
        if (key === '+' || key === '-') {
            key = '+/-';
        }

        if (key === '+/-' || key === 'Backspace' || key === 'Enter' || isDigit(key)) {
            refs.get(key).current.classList.remove('bg-[#00000022]', 'hover:shadow-md');
        }
    }

    React.useEffect(() => {        
        document.addEventListener('keydown', handleKeyDown);
        document.addEventListener('keyup', handleKeyUp);

        return () => {
            document.removeEventListener('keydown', handleKeyDown);
            document.removeEventListener('keyup', handleKeyUp);
        }
    }, [value, refs])

    return <Layout>
        <div className='flex flex-col grow'>
            <Transition
            as={React.Fragment}
            show={isShowing}
            enter="transform transition duration-[400ms]"
            enterFrom="opacity-0 translate-y-2/3 scale-50"
            enterTo="opacity-100 translate-y-0 scale-100"
            leave="transform duration-200 transition ease-in-out"
            leaveFrom="opacity-100 translate-y-0 scale-100 "
            leaveTo="opacity-0 scale-25"
            >
                <div className='my-5 mx-2 sm:mx-[10vw] md:mx-[20vw] xl:mx-[30vw] flex flex-col justify-between grow gap-[5vh]'>
                    <div className={`flex items-center ${classes}`}>
                        <div className={`flex-[3_3_0%] text-[#333333AA] text-center text-4xl sm:text-4xl md:text-6xl`}>
                            {tasks?.length > 0 && tasks[taskIndex].calculation}
                        </div>
                        <span className='text-[#333333AA] text-4xl sm:text-4xl md:text-6xl'>=</span>
                        <div className={`flex-[1_1_0%] text-[#333333AA] text-4xl sm:text-4xl md:text-6xl`}>
                            {value}
                        </div>
                    </div>   
                    <div className='grid grid-cols-4 gap-4 grow'>
                        <Button ref={refs.get('7')} value="7" onClick={changeValue}/>
                        <Button ref={refs.get('8')} value="8" onClick={changeValue}/>
                        <Button ref={refs.get('9')} value="9" onClick={changeValue}/>   
                        <Button ref={refs.get('Backspace')} value="⇦" onClick={removeDigit} className="row-span-2"/>
                        <Button ref={refs.get('4')} value="4" onClick={changeValue}/>
                        <Button ref={refs.get('5')} value="5" onClick={changeValue}/>
                        <Button ref={refs.get('6')} value="6" onClick={changeValue}/>                             
                        <Button ref={refs.get('1')} value="1" onClick={changeValue}/>
                        <Button ref={refs.get('2')} value="2" onClick={changeValue}/>
                        <Button ref={refs.get('3')} value="3" onClick={changeValue}/>
                        <Button ref={refs.get('Enter')} value="=" onClick={handleSetResult} className="row-span-2"/>
                        <Button ref={refs.get('0')} value="0" onClick={changeValue} className="col-span-2"/>
                        <Button ref={refs.get('+/-')} value="±" onClick={handlePlusMinus}/>
                        <div /><div />                                                                       
                    </div>                                       
                </div>
            </Transition>    
        </div>    
    </Layout>
};

export default Play;