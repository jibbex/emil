import React from 'react';
import { Routes, Route, useNavigate } from "react-router-dom";
import { Home, Settings, Play } from './views';
import { appReducer, initialState } from './state/reducer';
import './assets/App.css';


function App() {
    const navigate = useNavigate();
    const [state, dispatch] = React.useReducer(appReducer, initialState);
    const setTaskValue = (index, value) => dispatch({type: 'SET_TASK_VALUE', payload: {index, value}});
    const handleStart = () => {
        dispatch({type: 'CLEAR_TASKS'});

        for (let i = 0; i < state.presets.get(state.difficulty).taskCount; i++) {
            dispatch({type: 'ADD_TASK'});
        }        
        
        navigate(`/emil/play`);
    };    

    return (        
        <Routes>
            <Route path={`/emil/`} index element={<Home onStart={handleStart} />} />
            <Route path={`/emil/settings`} element={<Settings />} />
            <Route path={`/emil/play`} element={<Play tasks={state.tasks} setResult={setTaskValue} />} />
        </Routes>
    );
}

export default App;
