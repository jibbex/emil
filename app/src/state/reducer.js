const randNum = 
    ({min, max}) => Math.floor(Math.random() * (max - min) ) + min;

function calc(operator, a, b) {
    let result;

    switch(operator) {
        case '+':
            result = a + b;
            break;
        case '-':
            result = a - b;
            break;
        case '*':
            result = a * b;
            break;
        case '/':
            result = a / b;
            break;
    }

    return result;
}

function genCalculation({patterns}) {      
    let result = 0;  
    let lastOp = null;
    let operator = null;    
    const operands = [];    
    const patternIndex = randNum({min: 0, max: patterns.length});        
    const { operators, maxOperands, min, max } = patterns[patternIndex];    

    while(operands.length < maxOperands) {   
        const operandsCount = randNum({min:1,  max: maxOperands});        
        operator = operands.length % 2 === 0 ? operators[randNum({min: 0, max: operators.length})] : operator;

        if (operands.length % 2 === 0 && lastOp === operator)
            operator = operators.filter(o => o !== lastOp)[randNum({min: 0, max: operators.length - 1})];

        let num = randNum({min, max: Math.round(max / operandsCount + 1)});        

        if (operator === '/' && result % num !== 0) {
            operator = new Array('+', '-', '*')[randNum({min:0, max: 2})];
        }

        result = operator ? calc(operator, result, num) : num;
        
        while (result > max && result < min) {
            num = randNum({min, max: Math.round(max / operandsCount + 1)});
            result = calc(operator, result, num);
        }        
        
        lastOp = operator;
        operands.push({operator, num});       
    }

    return {
        calculation: operands.reduce((calculate, op, i) => {
            const val = calculate.val + ` ${i > 0 ? op.operator : ' '} ${op.num}`;
            return {
                lastOperator: op.operator,
                val
            };
        }, {val: ''}).val.trim(),
        result,
        value: null,
    };
}

const initialState = {
    useTimer: false,
    timer: 0,
    difficulty: 'easy',
    presets: new Map([
        [ 'easy', { 
            taskCount: 50,
            timer: {
                value: 0x1D4C0, // 120s
                bonus: 6000,
            },
            patterns: [{
                operators: ['+', '-'], 
                min: 0, 
                max: 30,             
                maxOperands: 2,
            }], 
        }], [ 'medium', { 
            taskCount: 100,
            timer: {
                value: 0x15F90, // 90s
                bonus: 4000 
            },
            patterns: [{
                operators: ['+', '-'], 
                min: 0, 
                max: 50,             
                maxOperands: 2,
            }, {
                operators: ['*', '/'], 
                min: 0, 
                max: 20,             
                maxOperands: 2,
            }]            
        }], [ 'hard', { 
            taskCount: 200,
            timer: {
                value: 0xEA60, // 60s
                bonus: 2000 
            },
            patterns: [{
                operators: ['+', '-'],
                min: 0, 
                max: 100,             
                maxOperands: 3,            
            }, {
                operators: ['*', '/'],
                min: 0, 
                max: 20,             
                maxOperands: 3,            
            }]
        }],
    ]),
    tasks: []
};

const appReducer = (state, action) => {
    let diff;

    switch(action.type) {
        case 'SET_DIFFICULTY': 
            return {
                ...state, 
                difficulty: action.payload.difficulty,
            };            
        case 'TOGGLE_TIMER':
            return {
                ...state,
                useTimer: !useTimer,
            };
        case 'ADD_TASK':
            diff = state.presets.get(state.difficulty);            
            return {
                ...state,
                tasks: [
                    ...state.tasks,
                    genCalculation(diff)
                ]
            };
        case 'SET_TASK_VALUE':
            diff = state.presets.get(state.difficulty);
            state.tasks[action.payload.index].value = action.payload.value;

            if (state.useTimer && state.tasks[action.payload.index].result === action.payload.value) {
                state.timer += diff.timer.bonus;
            }

            return state;
        case 'CLEAR_TASKS':
            return {
                ...state,
                tasks: [],
            };
        default:
            return state;
    }
};

export {appReducer, initialState};