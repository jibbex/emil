# Command Palette

This is the repository for the project demo built in [this article]().

### Development
To get started, clone this repository and switch to the `starter-project` branch.

#### Pre-requisites
This project uses Yarn, but if you're more familiar with Npm or Pnpm, you can delete the `yarn.lock` file before running `npm install` or `pnpm install`.

```sh
$ git clone https://github.com/Mayowa-Ojo/command-palette.git

$ git checkout starter-project

$ yarn

$ yarn dev
```